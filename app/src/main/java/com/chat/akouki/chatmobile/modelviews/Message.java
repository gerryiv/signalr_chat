package com.chat.akouki.chatmobile.modelviews;

import java.io.Serializable;

public class Message implements Serializable {
    public long MessageID;
    public long TemporaryID; //for pending messages
    public String Message;
    public String Status;
    //public String From;
    //public String Avatar;
    public boolean IsMine;
    public long SenderID;
    public String TimeStamp;
}
