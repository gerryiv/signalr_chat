package com.chat.akouki.chatmobile;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.chat.akouki.chatmobile.adapters.ChatMessageAdapter;
import com.chat.akouki.chatmobile.adapters.ChatRoomAdapter;
import com.chat.akouki.chatmobile.databinding.DialogMakeOfferBinding;
import com.chat.akouki.chatmobile.databinding.FragmentChatDetailBinding;
import com.chat.akouki.chatmobile.databinding.FragmentChatListBinding;
import com.chat.akouki.chatmobile.helpers.Globals;
import com.chat.akouki.chatmobile.helpers.User;
import com.chat.akouki.chatmobile.modelviews.ChatRoom;
import com.chat.akouki.chatmobile.modelviews.Message;
import com.chat.akouki.chatmobile.modelviews.PredefinedMessage;
import com.chat.akouki.chatmobile.modelviews.Product;
import com.chat.akouki.chatmobile.viewmodel.ChatRoomViewModel;
import com.chat.akouki.chatmobile.viewmodel.MessageViewModel;
import com.chat.akouki.chatmobile.viewmodel.PredefinedMessageViewModel;
import com.chat.akouki.chatmobile.viewmodel.ProductViewModel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;

import static android.content.ContentValues.TAG;

public class ChatListFragment extends Fragment {

    private ChatRoomViewModel mChatRooms;

    private int mCurrentPage = 1;
    private RecyclerView mRecyclerView;

    protected ViewDataBinding mBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mBinding = DataBindingUtil.inflate(
                LayoutInflater.from(container.getContext()),
                R.layout.fragment_chat_list,
                container,
                false);

        View currentView = this.mBinding.getRoot();

        this.postCreateViewHandler(savedInstanceState);

        this.mRecyclerView = currentView.findViewById(R.id.gvRecyclerChatRoom);

        this.setDefaultState();

        ((FragmentChatListBinding) this.mBinding).setViewModel(this.mChatRooms);

        return currentView;
    }

    public void onResume(){
        super.onResume();
        ChatActivity act = (ChatActivity) this.getActivity();
        act.chatService.Join(User.CurrentRoom);
    }

    public void onPause(){
        super.onPause();
        ChatActivity act = (ChatActivity) this.getActivity();
        act.chatService.Leave(User.CurrentRoom);
    }

    public void onDestroy(){
        super.onDestroy();
        ChatActivity act = (ChatActivity) this.getActivity();
        act.chatService.Leave(User.CurrentRoom);
    }

/*    @Override*/
    public void setDefaultState() {

        View view = this.getView();

        if(view != null && view.getVisibility() == View.VISIBLE) {
            Log.d("Fragment", this.getClass().getName());
            Activity act = this.getActivity();
        }
    }

    public void reconstructStateInstance(Bundle savedInstanceState) {
        if (savedInstanceState.getParcelable("ChatRoomViewModel") != null) {
            this.mChatRooms = savedInstanceState.getParcelable("ChatRoomViewModel");
            this.mChatRooms.setContext(getActivity());
        }
    }

    public void postCreateViewHandler(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            this.reconstructStateInstance(savedInstanceState);
        } else {
            Log.d(TAG, "postCreateViewHandler: initializeinstance");
            this.initialiazeInstance();
        }
    }

    public void initialiazeInstance() {

        final ChatRoomViewModel viewModel = new ChatRoomViewModel(getActivity()) {

            @Override
            public void loadMoreItem() {
                ChatListFragment.this.getRooms();
            }

        };
        this.mChatRooms = viewModel;

        this.getRooms();
    }

    private void getRooms() {

        ChatActivity act = (ChatActivity) this.getActivity();

        if(act.chatService!=null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    act.chatService.proxy.invoke(ChatRoom[].class, "GetRooms", User .UserID).done(
                            new Action<ChatRoom[]>() {
                                @Override
                                public void run(ChatRoom[] rooms) throws Exception {
                                    /*Globals.Rooms.clear();

                                    for (ChatRoom room : rooms)
                                        Globals.Rooms.add(String.valueOf(room.ChatRoomID));

                                    sendBroadcast(new Intent().setAction("notifyAdapter"));*/

                                    /*for (Message m : messageViewModels) {
                                        m.IsMine = m.SenderID == User.UserID ? 1 : 0;
                                    }*/

                                    if (mCurrentPage == 1) {
                                        LinkedList<ChatRoom> roomArrayList = new LinkedList<ChatRoom>();
                                        roomArrayList.addAll(Arrays.asList(rooms));
                                        mChatRooms.setItems(roomArrayList);
                                    } else {
                                        mChatRooms.addItems(Arrays.asList(rooms));
                                        ChatMessageAdapter adapter = (ChatMessageAdapter) mRecyclerView.getAdapter();

                                        if (adapter != null)
                                            adapter.notifyDataSetChanged();
                                    }

                                    if (Array.getLength(rooms) > 0) {
                                        mCurrentPage += 1;
                                    }

                                }
                            });
                    return null;
                }
            }.execute();
        }
    }

    public void notifyAdapter(){
        ChatRoomAdapter adapter = (ChatRoomAdapter) mRecyclerView.getAdapter();
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        this.generateBufferStateInstance(outState);
    }

    /*@Override*/
    public void generateBufferStateInstance(Bundle outState) {
        outState.putParcelable("ChatRoomViewModel", this.mChatRooms);
    }

}
