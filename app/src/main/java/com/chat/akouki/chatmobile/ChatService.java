package com.chat.akouki.chatmobile;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.chat.akouki.chatmobile.adapters.ChatMessageAdapter;
import com.chat.akouki.chatmobile.helpers.Globals;
import com.chat.akouki.chatmobile.helpers.User;
import com.chat.akouki.chatmobile.modelviews.Message;
import com.chat.akouki.chatmobile.modelviews.ChatRoom;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.Credentials;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.Logger;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.StateChangedCallback;
import microsoft.aspnet.signalr.client.http.Request;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler2;
import microsoft.aspnet.signalr.client.transport.ClientTransport;
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;

public class ChatService extends Service {

    private final IBinder mBinder = new LocalBinder();

    public HubConnection connection;
    public HubProxy proxy;
    private Handler handler;

    private String serverUrl = Globals.WEBURL;
    //private String serverUrl = "http://192.168.0.35/SnapIn";
    private String hubName = "ChatHub";
    public ConnectionState connectionState;

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // We are using binding. do we really need this...?
        if (!StartHubConnection()) {
            ExitWithMessage("Chat Service failed to start!");
        }
        if (!RegisterEvents()) {
            ExitWithMessage("End-point error: Failed to register Events!");
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        connection.stop();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if (connection == null) {
            if (!StartHubConnection()) {
                ExitWithMessage("Chat Service failed to start!");
            }

            if (!RegisterEvents()) {
                ExitWithMessage("End-point error: Failed to register Events!");
            }
        }
        return mBinder;
    }

    // https://developer.android.com/guide/components/bound-services.html
    public class LocalBinder extends Binder {
        public ChatService getService() {
            return ChatService.this;
        }
    }

    private boolean StartHubConnection() {
        Platform.loadPlatformComponent(new AndroidPlatformComponent());

        // Create Connection
        Logger logger = new Logger() {
            @Override
            public void log(String message, LogLevel level) {
                Log.d("Log Message : ", message);
            }
        };

        connection = new HubConnection(serverUrl, "", true, logger);

        // Create Proxy
        proxy = connection.createHubProxy(hubName);

        // Establish Connection
        ClientTransport clientTransport = new ServerSentEventsTransport(connection.getLogger());
        SignalRFuture<Void> signalRFuture = connection.start(clientTransport);

        handleConnectionState();

        connection.error(new ErrorCallback() {
            @Override
            public void onError(Throwable error) {
                error.printStackTrace();
            }
        });

        try {
            signalRFuture.get();
        } catch (InterruptedException e) {
            return false;
        } catch (ExecutionException e) {
            return false;
        }


        return true;
    }

    private void handleConnectionState(){
        //from https://github.com/SignalR/java-client/issues/92
        connection.stateChanged(new StateChangedCallback() {
            @Override
            public void stateChanged(ConnectionState oldState, ConnectionState newState) {
                try {
                    Timer timer = new Timer();

                    TimerTask reconnectTask = new TimerTask() {
                        @Override
                        public void run() {
                        Log.d("signalrconnection","ATTEMPTING RECONNECT");
                        try{
                            ClientTransport clientTransport = new ServerSentEventsTransport(connection.getLogger());
                            connection.start(clientTransport).get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            Log.d("signalrconnection","RECONNECT FAIL1: " + e.getMessage());
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                            Log.d("signalrconnection","RECONNECT FAIL2: " + e.getMessage());
                        } catch (Exception e){
                            e.printStackTrace();
                            Log.d("signalrconnection","RECONNECT FAIL3: " + e.getMessage());
                        }
                        }
                    };

                    connectionState = newState;

                    if (newState == ConnectionState.Connected) {
                        Log.d("signalrconnection", "STATE CHANGED-CONNECTED!");
                        timer.cancel();
                    } else if (newState == ConnectionState.Reconnecting) {
                        Log.d("signalrconnection", "STATE CHANGED-RECONNECTING!");

                        // should we reconnect here or on disconnect instead?
                        timer.scheduleAtFixedRate(reconnectTask, 1000, 5000);
                    } else if (newState == ConnectionState.Disconnected) {
                        Log.d("signalrconnection", "STATE CHANGED-DISCONNECTED!");
                    }
                }
                catch (Exception e){
                    Log.d("signalrconnection", "EXCEPTION: " + e.getMessage() + "|| STATE: " + newState.name());
                }
            }
        });
    }

    private boolean RegisterEvents() {

        Handler mHandler = new Handler(Looper.getMainLooper());
        try {
            //Handled in fragment
/*            proxy.on("addMessage", new SubscriptionHandler1<Message>() {
                @Override
                public void run(final Message msg) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            msg.IsMine = msg.SenderID == User.UserID ? 1 : 0;
                            Globals.Messages.add(msg);
                            sendBroadcast(new Intent().setAction("notifyAdapter"));
                        }
                    });
                }
            }, Message.class);*/

/*            proxy.on("newMessage", new SubscriptionHandler1<String>() {
                @Override
                public void run(final String msg) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Message msgViewModel = new Message();
                            msgViewModel.IsMine = 1;
                            msgViewModel.Message = msg;
                            msgViewModel.ProfilePicture = "";
                            Globals.Messages.add(msgViewModel);
                            sendBroadcast(new Intent().setAction("notifyAdapter"));
                        }
                    });
                }
            }, String.class);*/

            proxy.on("addChatRoom", new SubscriptionHandler1<ChatRoom>() {
                @Override
                public void run(final ChatRoom room) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Globals.Rooms.add(String.valueOf(room.ChatRoomID));
                            sendBroadcast(new Intent().setAction("notifyAdapter"));
                        }
                    });
                }
            }, ChatRoom.class);

            proxy.on("addChatRooms", new SubscriptionHandler1<ChatRoom[]>() {
                @Override
                public void run(final ChatRoom[] rooms) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Globals.Rooms.clear();

                            for (ChatRoom room : rooms)
                                Globals.Rooms.add(String.valueOf(room.ChatRoomID));

                            sendBroadcast(new Intent().setAction("notifyAdapter"));

/*                            Globals.Rooms.add(String.valueOf(room.ChatRoomID));
                            sendBroadcast(new Intent().setAction("notifyAdapter"));*/
                        }
                    });
                }
            }, ChatRoom[].class);

            proxy.on("addChatRoomID", new SubscriptionHandler1<Long>() {
                @Override
                public void run(final Long roomID) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Globals.Rooms.add(String.valueOf(roomID));
                            sendBroadcast(new Intent().setAction("notifyAdapter"));
                        }
                    });
                }
            }, Long.class);

            proxy.on("removeChatRoom", new SubscriptionHandler1<ChatRoom>() {
                @Override
                public void run(final ChatRoom room) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Globals.Rooms.remove(String.valueOf(room.ChatRoomID));
                            sendBroadcast(new Intent().setAction("notifyAdapter"));
                        }
                    });
                }
            }, ChatRoom.class);

            proxy.on("onError", new SubscriptionHandler1<String>() {
                @Override
                public void run(final String error) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ChatService.this, error, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });
                }
            }, String.class);

/*            proxy.on("onRoomDeleted", new SubscriptionHandler1<String>() {
                @Override
                public void run(final String info) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ChatService.this, info, Toast.LENGTH_LONG).show();
                                    sendBroadcast(new Intent().setAction("joinLobby"));
                                }
                            });
                        }
                    });
                }
            }, String.class);*/

        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public void Send(long roomID, String message, long tempID) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                proxy.invoke("Send", User.UserID, roomID, message, tempID);
                return null;
            }
        }.execute();
    }

    public void SendImage(long roomID, String message, String baseImage) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                proxy.invoke("SendImage", User.UserID, roomID, message, baseImage);
                return null;
            }
        }.execute();
    }

    public void Join(long roomID) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                User.CurrentRoom = roomID;
                proxy.invoke("Join", roomID);
                return null;
            }
        }.execute();
    }

    public void Leave(long roomID) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                User.CurrentRoom = roomID;
                proxy.invoke("Leave", roomID);
                return null;
            }
        }.execute();
    }

    public void CreateRoom(String roomName) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                proxy.invoke("CreateRoom", roomName);
                return null;
            }
        }.execute();
    }

    public void DeleteRoom(long roomID) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                proxy.invoke("DeleteRoom", roomID);
                return null;
            }
        }.execute();
    }

    public void DebugMessage(long userID, long roomID) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                proxy.invoke("DebugSendMessage", userID, roomID);
                return null;
            }
        }.execute();
    }

    public void DebugRead(long userID, long roomID) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                proxy.invoke("DebugReadMessage", userID, roomID);
                return null;
            }
        }.execute();
    }

    public void GetMessageHistory(long roomID) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                proxy.invoke(Message[].class, "GetMessageHistory", roomID, 1).done(
                        new Action<Message[]>() {
                            @Override
                            public void run(Message[] messageViewModels) throws Exception {
                                Globals.Messages.clear();
                                Globals.Messages.addAll(Arrays.asList(messageViewModels));

                                for (Message m : Globals.Messages) {
                                    m.IsMine = m.SenderID == User.UserID;
                                }
                                sendBroadcast(new Intent().setAction("notifyAdapter"));
                            }
                        });
                return null;
            }
        }.execute();
    }

    public void GetRooms() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                proxy.invoke(ChatRoom[].class, "GetRooms", User .UserID).done(
                        new Action<ChatRoom[]>() {
                            @Override
                            public void run(ChatRoom[] rooms) throws Exception {
                                Globals.Rooms.clear();

                                for (ChatRoom room : rooms)
                                    Globals.Rooms.add(String.valueOf(room.ChatRoomID));

                                sendBroadcast(new Intent().setAction("notifyAdapter"));
                            }
                        });
                return null;
            }
        }.execute();
    }

    public void GetUserRooms(){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                proxy.invoke("GetRooms", User.UserID);
                return null;
            }
        }.execute();


    }

    private void ExitWithMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                stopSelf();
            }
        }, 3000);
    }

}
