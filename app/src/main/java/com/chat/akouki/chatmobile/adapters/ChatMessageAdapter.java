package com.chat.akouki.chatmobile.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.chat.akouki.chatmobile.R;
import com.chat.akouki.chatmobile.viewmodel.ItemMessageViewModel;
import com.chat.akouki.chatmobile.databinding.ItemMessageMineBinding;
import com.chat.akouki.chatmobile.modelviews.Message;

import java.util.List;

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.BindingHolder> {

    private List<Message> mItems;
    private Context mContext;
    private int mLastPosition;
    public int mLastViewedPosition;

    public ChatMessageAdapter(Context ctx, List<Message> dataItems) {
        this.mItems = dataItems;
        this.mContext = ctx;
        this.mLastPosition = 0;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        BindingHolder holder = null;

        final ItemMessageMineBinding postBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_message_mine,
                parent,
                false);

        holder = new BindingHolder(postBinding);

        return holder;
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ViewDataBinding itemBinding = holder.binding;

        this.mLastViewedPosition = position;

        Message messageModel = mItems.get(position);

        ItemMessageViewModel itemViewModel = new ItemMessageViewModel(mContext, messageModel);
        ((ItemMessageMineBinding) itemBinding).setPosition(position);
        ((ItemMessageMineBinding) itemBinding).setViewModel(itemViewModel);

        int count = getItemCount() - 1;

        //count needs to be 15 or more so it wont keep triggering refresh if the room only has a few message
        if (position == count && this.mLastPosition != position && count > 15) {
            this.mLastPosition = position;
            this.loadMoreItem();
        }
        itemBinding.executePendingBindings();
    }

    public void resetLastPosition(){
        //just in case the last time loading message was failed, call this so you can load messages again
        this.mLastPosition = -1;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BindingHolder(ItemMessageMineBinding binding) {
            super(binding.messageItemMainContainer);
            this.binding = binding;
        }
    }

    protected void loadMoreItem() {
        //Handle in UI
    }
}
