package com.chat.akouki.chatmobile.viewmodel;

import android.content.Context;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.Gravity;

import com.chat.akouki.chatmobile.BR;
import com.chat.akouki.chatmobile.modelviews.ChatRoom;
import com.chat.akouki.chatmobile.modelviews.Message;

public class RoomViewModel extends BaseViewModel implements Parcelable {

    public RoomViewModel(Context context) {
        super(context);
    }

    public RoomViewModel(Context context, ChatRoom itemModel) {
        super(context);

        this.setItemModel(itemModel);
    }

    public RoomViewModel(Parcel in) {

        super(null);

        this.setItemModel((ChatRoom) in.readValue(RoomViewModel.class.getClassLoader()));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.getItemModel());
    }

    public static final Creator CREATOR = new Creator() {
        public RoomViewModel createFromParcel(Parcel in) {
            return new RoomViewModel(in);
        }

        public RoomViewModel[] newArray(int size) {
            return new RoomViewModel[size];
        }
    };

    @Bindable
    public ChatRoom itemModel;

    public ChatRoom getItemModel() {
        return this.itemModel;
    }

    public void setItemModel(ChatRoom roomItemModel) {
        this.itemModel = roomItemModel;
        this.notifyPropertyChanged(BR._all);
    }

    @Bindable
    public long getID() {
        ChatRoom chatRoom = this.getItemModel();
        return chatRoom.ChatRoomID;
    }
}
