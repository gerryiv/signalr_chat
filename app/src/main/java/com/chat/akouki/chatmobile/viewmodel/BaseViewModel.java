package com.chat.akouki.chatmobile.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;

public abstract class BaseViewModel extends BaseObservable {

    protected Context mContext;

    public BaseViewModel () {}

    public BaseViewModel (Context context)
    {
        this.mContext = context;
    }

    public void setContext(Context context)
    {
        this.mContext = context;
    }

}