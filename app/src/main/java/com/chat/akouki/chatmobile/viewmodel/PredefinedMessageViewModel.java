package com.chat.akouki.chatmobile.viewmodel;


import android.content.Context;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.chat.akouki.chatmobile.BR;
import com.chat.akouki.chatmobile.R;
import com.chat.akouki.chatmobile.adapters.ChatMessageAdapter;
import com.chat.akouki.chatmobile.adapters.PredefinedMessageAdapter;
import com.chat.akouki.chatmobile.modelviews.Message;
import com.chat.akouki.chatmobile.modelviews.PredefinedMessage;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class PredefinedMessageViewModel extends BaseViewModel implements Parcelable {

    public PredefinedMessageViewModel(Context context) {
        super(context);
        this.mContext = context;
    }

    protected PredefinedMessageViewModel(Parcel in) {
        super(null);

        this.setItems((List<PredefinedMessage>) in.readValue(Collection.class.getClassLoader()));
    }

    public static final Creator CREATOR = new Creator() {

        public PredefinedMessageViewModel createFromParcel(Parcel in) {return new PredefinedMessageViewModel(in);}

        public PredefinedMessageViewModel[] newArray(int size) {return new PredefinedMessageViewModel[size];}
    };


    @Bindable
    private List<PredefinedMessage> items;

    public List<PredefinedMessage> getItems() {
        return items;
    }

    public void setItems(List<PredefinedMessage> data){
        this.items = data;
        this.notifyChange();
    }

    public void addItems(List<PredefinedMessage> data){
        for (PredefinedMessage m: data ) {
            this.getItems().add(m);
        }
        //this.notifyChange();
    }

    public void loadMoreItem() {
        //Override in UI
    }

    @BindingAdapter({"android:predefMessageItems"})
    public static void bindMessage(RecyclerView view, final PredefinedMessageViewModel model) {
        view.setHasFixedSize(false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        view.setLayoutManager(layoutManager);

        if (model.getItems() != null) {
            view.setAdapter(new PredefinedMessageAdapter(view.getContext(), model.getItems()));
            view.scrollToPosition(0);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.getItems());
    }
}
