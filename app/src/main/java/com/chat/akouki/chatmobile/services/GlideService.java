package com.chat.akouki.chatmobile.services;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.chat.akouki.chatmobile.R;
import com.chat.akouki.chatmobile.helpers.RoundImageTransform;

import java.lang.reflect.Field;

/**
 * Created by user on 5/4/2016.
 */
public class GlideService {

    public final static int FIT_CENTER = 1;
    public final static int CENTER_CROP = 2;
    public final static int CENTER_INSIDE = 3;

    private String mDefaultImage;
    private Drawable mDefaultDrawable;
    final private Context mContext;

    public GlideService(Context context)
    {
        this(context, "");
    }

    public GlideService(Context context, String defaultImage)
    {
        this.mContext = context;
        this.mDefaultImage = defaultImage;
        this.mDefaultDrawable = null;
    }

    public GlideService(Context context, Drawable defaultDrawable)
    {
        this.mContext = context;
        this.mDefaultImage = "";
        this.mDefaultDrawable = defaultDrawable;
    }

    public void loadImage(ImageView view, Uri imageUri)
    {
        loadImage(view, imageUri, 0);
    }

    public void loadCircularImage(ImageView view, Uri imageUri, int scaleType){

        getRoundImageRequest(view, imageUri, scaleType, 0).into(view);
    }

    public void loadImage(final ImageView view, Uri imageUri, int scaleType) {
        getImageRequest(view, imageUri, scaleType, 0).into(view);
    }

    public void loadImage(final ImageView view, Uri imageUri, int scaleType, int fixedHeight)
    {
        Log.d("GlideService", "FixedHeight: " + String.valueOf(fixedHeight));

        view.layout(0, 0, 0, 0);
        getImageRequest(view, imageUri, scaleType, fixedHeight).into(view);
    }

    public RequestBuilder<Drawable> getRoundImageRequest(final ImageView view, Uri imageUri, int scaleType, final int fixedHeight)
    {
        int defaultImageId = 0;

        if(!TextUtils.isEmpty(this.mDefaultImage)) {
            try {
                Class res = R.drawable.class;
                Field field = res.getField(this.mDefaultImage);
                defaultImageId = field.getInt(null);
            } catch (NoSuchFieldException e) {
                //Do nothing
            } catch (IllegalAccessException e) {
                //Do nothing
            }
        }

        RequestOptions glideOptions = new RequestOptions();

        if(defaultImageId > 0)
        {
            glideOptions.placeholder(defaultImageId);
        }
        else if(this.mDefaultDrawable != null)
        {
            glideOptions.placeholder(this.mDefaultDrawable);
        }

        glideOptions.transform(new RoundImageTransform(mContext));

        switch(scaleType) {
            case CENTER_CROP:
                glideOptions.centerCrop();
                break;
            case CENTER_INSIDE:
                glideOptions.centerInside();
                break;
            case FIT_CENTER:
            default:
                glideOptions.fitCenter();
                break;
        }

        RequestBuilder<Drawable> req = Glide.with(this.mContext)
                .load(imageUri).apply(glideOptions);

        //req.into(view);

        req.listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                if(fixedHeight > 0 && mDefaultDrawable != null) {
                    double ratio = (double) mDefaultDrawable.getIntrinsicWidth() / (double) mDefaultDrawable.getIntrinsicHeight();

                    view.getLayoutParams().height = (int) Math.ceil(ratio * fixedHeight);
                    view.requestLayout();
                }

                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                if(fixedHeight > 0) {
                    double ratio = (double) resource.getIntrinsicWidth() / (double) resource.getIntrinsicHeight();

                    view.getLayoutParams().width = (int) Math.ceil(ratio * fixedHeight);
                    view.requestLayout();
                }

                return false;
            }
        });

        return req;
    }

    public RequestBuilder<Drawable> getImageRequest(final ImageView view, Uri imageUri, int scaleType, final int fixedHeight)
    {
        int defaultImageId = 0;

        if(!TextUtils.isEmpty(this.mDefaultImage)) {
            try {
                Class res = R.drawable.class;
                Field field = res.getField(this.mDefaultImage);
                defaultImageId = field.getInt(null);
            } catch (NoSuchFieldException e) {
                //Do nothing
            } catch (IllegalAccessException e) {
                //Do nothing
            }
        }

        RequestOptions glideOptions = new RequestOptions();

        if(defaultImageId > 0)
        {
            glideOptions.placeholder(defaultImageId);
        }
        else if(this.mDefaultDrawable != null)
        {
            glideOptions.placeholder(this.mDefaultDrawable);
        }

        switch(scaleType) {
            case CENTER_CROP:
                glideOptions.centerCrop();
                break;
            case CENTER_INSIDE:
                glideOptions.centerInside();
                break;
            case FIT_CENTER:
            default:
                glideOptions.fitCenter();
                break;
        }

        RequestBuilder<Drawable> req = Glide.with(this.mContext)
                .load(imageUri).apply(glideOptions);

        //req.into(view);

        req.listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                //if(fixedHeight > 0 && mDefaultDrawable != null) {
                //    double ratio = (double) mDefaultDrawable.getIntrinsicWidth() / (double) mDefaultDrawable.getIntrinsicHeight();

                //    view.getLayoutParams().height = (int) Math.ceil(ratio * fixedHeight);
                //    view.requestLayout();
                //}

                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                if(fixedHeight > 0) {
                    double ratio = (double) resource.getIntrinsicWidth() / (double) resource.getIntrinsicHeight();

                    view.getLayoutParams().width = (int) Math.ceil(ratio * fixedHeight);
                    view.requestLayout();
                }

                return false;
            }
        });

        return req;
    }

    public void loadImageWidth(final ImageView view, Uri imageUri, int scaleType, int fixedWidth)
    {
        view.layout(0, 0, 0, 0);
        getImageRequestWidth(view, imageUri, scaleType, fixedWidth).into(view);
    }

    public RequestBuilder<Drawable> getImageRequestWidth(final ImageView view, Uri imageUri, int scaleType, final int fixedWidth)
    {
        int defaultImageId = 0;

        if(!TextUtils.isEmpty(this.mDefaultImage)) {
            try {
                Class res = R.drawable.class;
                Field field = res.getField(this.mDefaultImage);
                defaultImageId = field.getInt(null);
            } catch (NoSuchFieldException e) {
                //Do nothing
            } catch (IllegalAccessException e) {
                //Do nothing
            }
        }

        RequestOptions glideOptions = new RequestOptions();

        if(defaultImageId > 0)
        {
            glideOptions.placeholder(defaultImageId);
        }
        else if(this.mDefaultDrawable != null)
        {
            glideOptions.placeholder(this.mDefaultDrawable);
        }

        switch(scaleType) {
            case CENTER_CROP:
                glideOptions.centerCrop();
                break;
            case CENTER_INSIDE:
                glideOptions.centerInside();
                break;
            case FIT_CENTER:
            default:
                glideOptions.fitCenter();
                break;
        }

        RequestBuilder<Drawable> req = Glide.with(this.mContext)
                .load(imageUri).apply(glideOptions);

        req.listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                //if(fixedHeight > 0 && mDefaultDrawable != null) {
                //    double ratio = (double) mDefaultDrawable.getIntrinsicWidth() / (double) mDefaultDrawable.getIntrinsicHeight();

                //    view.getLayoutParams().height = (int) Math.ceil(ratio * fixedHeight);
                //    view.requestLayout();
                //}

                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                if(fixedWidth > 0) {
                    double ratio = (double) resource.getIntrinsicHeight() / (double) resource.getIntrinsicWidth();

                    view.getLayoutParams().height = (int) Math.ceil(ratio * fixedWidth);
                    view.requestLayout();
                }

                return false;
            }
        });

        return req;
    }
}
