package com.chat.akouki.chatmobile.viewmodel;

import android.content.Context;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import com.chat.akouki.chatmobile.BR;
import com.chat.akouki.chatmobile.R;
import com.chat.akouki.chatmobile.services.GlideService;
import com.chat.akouki.chatmobile.modelviews.Message;

public class ItemMessageViewModel extends BaseViewModel implements Parcelable {

    public ItemMessageViewModel(Context context) {
        super(context);
    }

    public ItemMessageViewModel(Context context, Message itemModel) {
        super(context);

        this.setItemModel(itemModel);
    }

    public ItemMessageViewModel(Parcel in) {

        super(null);

        this.setItemModel((Message) in.readValue(ItemMessageViewModel.class.getClassLoader()));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.getItemModel());
    }

    public static final Creator CREATOR = new Creator() {
        public ItemMessageViewModel createFromParcel(Parcel in) {
            return new ItemMessageViewModel(in);
        }

        public ItemMessageViewModel[] newArray(int size) {
            return new ItemMessageViewModel[size];
        }
    };

    @Bindable
    private Message itemModel;

    private Message getItemModel() {
        return this.itemModel;
    }

    private void setItemModel(Message messageItemModel) {
        this.itemModel = messageItemModel;
        this.notifyPropertyChanged(BR._all);
    }

    public String getContent() {
        Message message = this.getItemModel();
        return message.Message;
    }

    @Bindable
    public String getMessageText(){
        String message = this.getContent();

        if(message.startsWith("::IMG::")) {
            String[] messageComps = message.split("::", 4);
            return messageComps[3];
        }
        else
            return message;
    }

    @Bindable
    public boolean getHasImage(){
        //message would be like: "::IMG::{url}::{message}"
        return getContent().startsWith("::IMG::");
    }

    @BindingAdapter({"statusIcon"})
    public static void setStatusIcon(ImageView imageView, ItemMessageViewModel model) {
        Context context = imageView.getContext();
        Drawable drawable = null;

        switch (model.getStatus()) {
            case "R":
                //With this you probably don't actually need the ic_read file? Can just use ic_delivered
                drawable = ContextCompat.getDrawable(context, R.drawable.ic_read);
                drawable.setColorFilter(context.getResources().getColor(R.color.msgStatusRead), PorterDuff.Mode.SRC_IN);
                break;
            case "D":
                drawable = ContextCompat.getDrawable(context, R.drawable.ic_delivered);
                break;
            case "S":
                drawable = ContextCompat.getDrawable(context, R.drawable.ic_sent);
                break;
            case "P":
                drawable = ContextCompat.getDrawable(context, R.drawable.ic_pending);
                break;
            default:
                break;
        }

        if (drawable != null) {
            imageView.setImageDrawable(drawable);
        }
    }

    @BindingAdapter({"fixedHeightImageUrl"})
    public static void setFixedHeightImageUrl(ImageView imageView, ItemMessageViewModel model) {
        if (model.getHasImage()) {
            //prevent image from resizing when loading previous messages
            imageView.requestLayout();
            imageView.getLayoutParams().height = 400;

            Context context = imageView.getContext();
/*
            Drawable defaultImage = ContextCompat.getDrawable(context, R.drawable.ic_menu_gallery);
            Drawable wrapDrawable = DrawableCompat.wrap(defaultImage);
            DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(context, R.color.cardview_light_background));
*/

            //Load image bg
            Uri bgImageUri = Uri.parse(model.getImageInMessage());

            GlideService service = new GlideService(context/*, wrapDrawable*/);
            service.loadImage(imageView, bgImageUri, GlideService.FIT_CENTER, 400);
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }
    }

    private String getImageInMessage(){
        //message would be like: "::IMG::{url}::{message}"
        String message = getContent();
        String[] messageComps = message.split("::", 4);

        return messageComps[2];
    }

/*    @Bindable
    public String getUser() {
        Message message = this.getItemModel();
        return message.From;
    }*/

    @Bindable
    public String getTimestamp() {
        Message message = this.getItemModel();
        return message.TimeStamp;
    }

    @Bindable
    public int getGravity() {
        Message message = this.getItemModel();

        return message.IsMine ? Gravity.RIGHT : Gravity.LEFT;
    }

    @Bindable
    public boolean getIsMine() {
        Message message = this.getItemModel();
        return message.IsMine;
    }

    @Bindable
    public String getStatus() {
        Message message = this.getItemModel();

        if (message.Status != null) {
            return message.Status;
        }
        return "";
    }
}
