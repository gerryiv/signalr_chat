package com.chat.akouki.chatmobile.viewmodel;


import android.content.Context;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.chat.akouki.chatmobile.BR;
import com.chat.akouki.chatmobile.R;
import com.chat.akouki.chatmobile.adapters.ChatMessageAdapter;
import com.chat.akouki.chatmobile.modelviews.Message;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class MessageViewModel extends BaseViewModel implements Parcelable {

    public MessageViewModel(Context context) {
        super(context);
        this.mContext = context;
    }

    protected MessageViewModel(Parcel in) {
        super(null);

        this.setItems((LinkedList<Message>) in.readValue(Collection.class.getClassLoader()));
    }

    public static final Creator CREATOR = new Creator() {

        public MessageViewModel createFromParcel(Parcel in) {return new MessageViewModel(in);}

        public MessageViewModel[] newArray(int size) {return new MessageViewModel[size];}
    };

    @Bindable
    private ArrayList<Bitmap> bitmaps;

    public ArrayList<Bitmap> getBitmaps(){
        return bitmaps;
    }

    public Bitmap getFirstBitmap(){
        return bitmaps.get(0);
    }

    @Bindable
    private LinkedList<Message> items;

    public LinkedList<Message> getItems() {
        return items;
    }

    public void setItems(LinkedList<Message> data){
        this.items = data;
        this.notifyChange();
    }

    public void addItemsFirst(List<Message> data){
        for (Message m: data ) {
            this.getItems().addFirst(m);
        }
        this.notifyChange();
    }

    public void addItemFirst(Message data){
        this.getItems().addFirst(data);
        //this.notifyChange();
    }

    public void addItems(List<Message> data){
        for (Message m: data ) {
            this.getItems().add(m);
        }
    }

    public void addBitmap(Bitmap bitmap){
        bitmaps.add(bitmap);
        this.notifyChange();
    }

    public void removeBitmaps(View view){
        removeBitmaps();
    }

    public void removeBitmaps(){
        bitmaps = new ArrayList<>();
        this.notifyChange();
    }

    @BindingAdapter({"previewImage"})
    public static void setPreviewImage(ImageView imageView, MessageViewModel model) {
        if(model.bitmaps != null && !model.bitmaps.isEmpty())
            imageView.setImageBitmap(model.bitmaps.get(0));
        else
            imageView.setImageBitmap(null);
    }

    @Bindable
    public boolean getHasImage(){
        return bitmaps != null && !bitmaps.isEmpty();
    }

    public void loadMoreItem() {
        //Override in UI
    }

    public void setToRead(Long[] messageIDs){
        List<Long> IDList = Arrays.asList(messageIDs);
        for (Message m : this.getItems()){
            if (IDList.contains(m.MessageID))
                m.Status = "R";
        }
        this.notifyPropertyChanged(BR.status);
        this.notifyChange();
    }

    @BindingAdapter({"android:messageItems"})
    public static void bindMessage(RecyclerView view, final MessageViewModel model) {
        view.setHasFixedSize(false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        view.setLayoutManager(layoutManager);

        if (model.getItems() != null) {
            view.setAdapter(new ChatMessageAdapter(view.getContext(), model.getItems()) {
                @Override
                protected void loadMoreItem() {
                    model.loadMoreItem();
                }
            });
            view.scrollToPosition(0);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.getItems());
    }
}
