package com.chat.akouki.chatmobile.helpers;

import com.chat.akouki.chatmobile.modelviews.Message;

import java.util.ArrayList;

public class Globals {
    public static ArrayList<Message> Messages = new ArrayList<>();
    public static ArrayList<String> Rooms = new ArrayList<>();

    public static final int PICK_IMAGE = 10001;
    public static final int PERMISSION_EXTERNAL = 10002;

    //final public static String WEBURL = "http://staging2.jadetechsolutions.com/SnapIn";
    final public static String WEBURL = "http://192.168.0.35/SnapIn";
}
