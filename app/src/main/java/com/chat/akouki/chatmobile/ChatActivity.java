package com.chat.akouki.chatmobile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.chat.akouki.chatmobile.adapters.ChatMessageAdapter;
import com.chat.akouki.chatmobile.adapters.MessageAdapter;
import com.chat.akouki.chatmobile.helpers.Globals;
import com.chat.akouki.chatmobile.helpers.User;
import com.chat.akouki.chatmobile.viewmodel.MessageViewModel;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.chat.akouki.chatmobile.helpers.Globals.PICK_IMAGE;

public class ChatActivity extends AppCompatActivity {

    // Used to receive messages from ChatService
    MyReceiver myReceiver;

    // Chat Service
    ChatService chatService;
    boolean mBound = false;

    //MessageAdapter adapter;
    //ChatMessageAdapter chatAdapter;

    MessageViewModel mChatMessages;

    @Override
    protected void onStart() {
        super.onStart();
        if(!mBound) {
            Intent intent = new Intent(this, ChatService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

            //Register events we want to receive from Chat Service
            myReceiver = new MyReceiver();
            IntentFilter intentFilter = new IntentFilter();
            //intentFilter.addAction("newMessage");
            intentFilter.addAction("notifyAdapter");
            //intentFilter.addAction("joinLobby");
            registerReceiver(myReceiver, intentFilter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver);
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    protected void getMessageFragment(){
        Fragment fragment = new ChatDetailFragment();
        FragmentManager fragmentManager = this.getSupportFragmentManager();

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.chat_container, fragment);
        transaction.show(fragment);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(String.valueOf(User.CurrentRoom));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_add:
                //For debugging purposes: Get other party to send a debug message
                chatService.DebugMessage(User.UserID, User.CurrentRoom);
                break;
            case R.id.action_read_message:
                //For debugging purposes: Get other party to read the chat
                chatService.DebugRead(User.UserID, User.CurrentRoom);
                break;
            case R.id.action_open_menu:
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(ChatActivity.this, findViewById(R.id.action_open_menu));
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.chat_detail_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(
                                ChatActivity.this,
                                "Clicked " + item.getTitle(),
                                Toast.LENGTH_SHORT
                        ).show();
                        return true;
                    }
                });
                popup.show(); //showing popup menu

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Globals.Messages.clear();
        super.onBackPressed();
    }

    //https://developer.android.com/guide/components/bound-services.html
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            ChatService.LocalBinder binder = (ChatService.LocalBinder) service;
            chatService = binder.getService();
            mBound = true;
            getMessageFragment();
            //fix this
            // chatService.GetMessageHistory(User.CurrentRoom);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            switch (intent.getAction()) {
                case "notifyAdapter":
                    ChatDetailFragment fragment = (ChatDetailFragment) getSupportFragmentManager().findFragmentById(R.id.chat_container);

                    if(fragment != null)
                        //fragment.notifyAdapter();
                    /*if (chatAdapter!=null)
                        chatAdapter.notifyDataSetChanged();*/
                    //adapter.notifyDataSetChanged();
                    break;
/*                case "joinLobby":
                    User.CurrentRoom = 0;
                    chatService.Join(User.CurrentRoom);
                    chatService.GetMessageHistory(User.CurrentRoom);
                    getSupportActionBar().setTitle("Lobby");
                    break;*/
            }
        }
    } // MyReceiver
}
