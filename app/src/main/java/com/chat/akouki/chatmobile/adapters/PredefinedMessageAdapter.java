package com.chat.akouki.chatmobile.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.chat.akouki.chatmobile.BR;
import com.chat.akouki.chatmobile.R;
import com.chat.akouki.chatmobile.databinding.ItemMessageMineBinding;
import com.chat.akouki.chatmobile.databinding.ItemPredefinedMessageBinding;
import com.chat.akouki.chatmobile.modelviews.Message;
import com.chat.akouki.chatmobile.modelviews.PredefinedMessage;
import com.chat.akouki.chatmobile.viewmodel.ItemMessageViewModel;
import com.chat.akouki.chatmobile.viewmodel.ItemPredefinedMessageViewModel;

import java.util.List;

public class PredefinedMessageAdapter extends RecyclerView.Adapter<PredefinedMessageAdapter.BindingHolder> {

    private List<PredefinedMessage> mItems;
    private Context mContext;
    private int mLastPosition;
    private ItemPredefinedMessageViewModel mLastChoice;

    public PredefinedMessageAdapter(Context ctx, List<PredefinedMessage> dataItems) {
        this.mItems = dataItems;
        this.mContext = ctx;
        this.mLastPosition = 0;
        this.mLastChoice = null;//new ItemPredefinedMessageViewModel(ctx);
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        BindingHolder holder = null;

        final ItemPredefinedMessageBinding postBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_predefined_message,
                parent,
                false);

        holder = new BindingHolder(postBinding);

        return holder;
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ViewDataBinding itemBinding = holder.binding;

        PredefinedMessage predefinedMessageModel = mItems.get(position);

        ItemPredefinedMessageViewModel itemViewModel = new ItemPredefinedMessageViewModel(mContext, predefinedMessageModel){
            @Override
            public void setIsSelected(boolean selected){
                super.setIsSelected(selected);

                if (selected && mLastChoice != this) {
                    if (mLastChoice != null) {
                        mLastChoice.setIsSelected(false);
                        mLastChoice.notifyPropertyChanged(BR.isSelected);
                        //mLastChoice.notifyPropertyChanged(BR.isSelected);
                    }
                    mLastChoice = this;
                }
            }
        };
        ((ItemPredefinedMessageBinding) itemBinding).setPosition(position);
        ((ItemPredefinedMessageBinding) itemBinding).setViewModel(itemViewModel);


        /*((ItemPredefinedMessageBinding) itemBinding).predefMessageContent.setOnClickListener(v -> {
            EditText editText = (EditText) ((View)v.getParent()).findViewById(R.id.editMessage);
            editText.setText(((Button)v).getText());
        });*/

        itemBinding.executePendingBindings();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BindingHolder(ItemPredefinedMessageBinding binding) {
            super(binding.predefMessageItemMainContainer);
            this.binding = binding;
        }
    }

    protected void loadMoreItem() {
        //Handle in UI
    }
}
