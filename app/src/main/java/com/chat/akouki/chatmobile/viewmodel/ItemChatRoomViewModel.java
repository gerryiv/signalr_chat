package com.chat.akouki.chatmobile.viewmodel;

import android.content.Context;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.Gravity;

import com.chat.akouki.chatmobile.BR;
import com.chat.akouki.chatmobile.modelviews.ChatRoom;
import com.chat.akouki.chatmobile.modelviews.Message;

public class ItemChatRoomViewModel extends BaseViewModel implements Parcelable {

    public ItemChatRoomViewModel(Context context) {
        super(context);
    }

    public ItemChatRoomViewModel(Context context, ChatRoom itemModel) {
        super(context);

        this.setItemModel(itemModel);
    }

    public ItemChatRoomViewModel(Parcel in) {

        super(null);

        this.setItemModel((ChatRoom) in.readValue(ItemChatRoomViewModel.class.getClassLoader()));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.getItemModel());
    }

    public static final Creator CREATOR = new Creator() {
        public ItemChatRoomViewModel createFromParcel(Parcel in) {
            return new ItemChatRoomViewModel(in);
        }

        public ItemChatRoomViewModel[] newArray(int size) {
            return new ItemChatRoomViewModel[size];
        }
    };

    @Bindable
    public ChatRoom itemModel;

    public ChatRoom getItemModel() {
        return this.itemModel;
    }

    public void setItemModel(ChatRoom chatRoomItemModel) {
        this.itemModel = chatRoomItemModel;
        this.notifyPropertyChanged(BR._all);
/*
        this.notifyPropertyChanged(BR.content);
        this.notifyPropertyChanged(BR.title);
        this.notifyPropertyChanged(BR.preview);
        this.notifyPropertyChanged(BR.createdDate);
*/
    }

    @Bindable
    public String getID() {
        ChatRoom chatRoom = this.getItemModel();
        return String.valueOf(chatRoom.ChatRoomID);
    }

}
