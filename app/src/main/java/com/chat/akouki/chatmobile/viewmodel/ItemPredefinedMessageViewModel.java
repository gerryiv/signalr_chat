package com.chat.akouki.chatmobile.viewmodel;

import android.content.Context;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.chat.akouki.chatmobile.BR;
import com.chat.akouki.chatmobile.R;
import com.chat.akouki.chatmobile.modelviews.Message;
import com.chat.akouki.chatmobile.modelviews.PredefinedMessage;

public class ItemPredefinedMessageViewModel extends BaseViewModel implements Parcelable {

    public ItemPredefinedMessageViewModel(Context context) {
        super(context);
    }

    public ItemPredefinedMessageViewModel(Context context, PredefinedMessage itemModel) {
        super(context);

        this.setItemModel(itemModel);
    }

    public ItemPredefinedMessageViewModel(Parcel in) {

        super(null);

        this.setItemModel((PredefinedMessage) in.readValue(ItemPredefinedMessageViewModel.class.getClassLoader()));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.getItemModel());
    }

    public static final Creator CREATOR = new Creator() {
        public ItemPredefinedMessageViewModel createFromParcel(Parcel in) {
            return new ItemPredefinedMessageViewModel(in);
        }

        public ItemPredefinedMessageViewModel[] newArray(int size) {
            return new ItemPredefinedMessageViewModel[size];
        }
    };

    @Bindable
    public PredefinedMessage itemModel;

    public PredefinedMessage getItemModel() {
        return this.itemModel;
    }

    public void setItemModel(PredefinedMessage messageItemModel) {
        this.itemModel = messageItemModel;
        this.itemModel.isSelected = false;
        this.notifyPropertyChanged(BR._all);
    }

    @Bindable
    public boolean getIsSelected(){
        return this.getItemModel().isSelected;
    }

    public void setIsSelected(boolean selected){
        this.getItemModel().isSelected = selected;
        this.notifyPropertyChanged(BR.isSelected);
    }

    @Bindable
    public String getMessage() {
        PredefinedMessage message = this.getItemModel();
        return message.Message;
    }

    public void onClickPredif(View v){
        //TODO: NEED TO FIX THIS
        setIsSelected(true);
    }
}
