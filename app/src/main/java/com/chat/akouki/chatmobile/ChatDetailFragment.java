package com.chat.akouki.chatmobile;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.chat.akouki.chatmobile.adapters.ChatMessageAdapter;
import com.chat.akouki.chatmobile.databinding.DialogMakeOfferBinding;
import com.chat.akouki.chatmobile.databinding.DialogPredefinedMessagesListBinding;
import com.chat.akouki.chatmobile.databinding.FragmentChatDetailBinding;
import com.chat.akouki.chatmobile.helpers.FileUtils;
import com.chat.akouki.chatmobile.helpers.Globals;
import com.chat.akouki.chatmobile.helpers.User;
import com.chat.akouki.chatmobile.modelviews.Message;
import com.chat.akouki.chatmobile.modelviews.PredefinedMessage;
import com.chat.akouki.chatmobile.modelviews.Product;
import com.chat.akouki.chatmobile.viewmodel.MessageViewModel;
import com.chat.akouki.chatmobile.viewmodel.PredefinedMessageViewModel;
import com.chat.akouki.chatmobile.viewmodel.ProductViewModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.StateChangedCallback;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;
import microsoft.aspnet.signalr.client.transport.ClientTransport;
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;

import static android.content.ContentValues.TAG;
import static com.chat.akouki.chatmobile.helpers.Globals.PICK_IMAGE;

public class ChatDetailFragment extends Fragment {

    private MessageViewModel mMessage;
    private ProductViewModel mProduct;
    private PredefinedMessageViewModel mPredefMessage;

    private int mCurrentPage = 1;
    private RecyclerView mRecyclerView;
    //private HashMap<String, Object> mData;
/*    private ArrayList<ODataQueryBaseOption> mFilter;
    private ArrayList<ODataQueryOption> mSort;*/

    protected ViewDataBinding mBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mBinding = DataBindingUtil.inflate(
                LayoutInflater.from(container.getContext()),
                R.layout.fragment_chat_detail,
                container,
                false);

        View currentView = this.mBinding.getRoot();

        this.postCreateViewHandler(savedInstanceState);

        this.mRecyclerView = currentView.findViewById(R.id.gvRecyclerMessages);

        this.setDefaultState();

        ((FragmentChatDetailBinding) this.mBinding).setViewModel(this.mMessage);
        ((FragmentChatDetailBinding) this.mBinding).setProductViewModel(this.mProduct);
        ((FragmentChatDetailBinding) this.mBinding).setPredefViewModel(this.mPredefMessage);

        setBtnListeners(currentView);
        handleAddMsg();

        this.mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    mRecyclerView.smoothScrollToPosition(0);
                    mProduct.setKeyboardDown(false);
                }
                else if(bottom > oldBottom){
                    mProduct.setKeyboardDown(true);
                }
            }
        });

        return currentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        handleConnectionState();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        ChatActivity act = (ChatActivity) this.getActivity();
        act.chatService.Leave(User.CurrentRoom);
    }

    public void setDefaultState() {
        View view = this.getView();

        if(view != null && view.getVisibility() == View.VISIBLE) {
            Log.d("Fragment", this.getClass().getName());
            Activity act = this.getActivity();
        }
    }

    public void reconstructStateInstance(Bundle savedInstanceState) {
        if (savedInstanceState.getParcelable("MessageViewModel") != null) {
            this.mMessage = savedInstanceState.getParcelable("MessageViewModel");
            this.mMessage.setContext(getActivity());
        }
        if (savedInstanceState.getParcelable("ProductViewModel") != null) {
            this.mProduct = savedInstanceState.getParcelable("ProductViewModel");
            this.mProduct.setContext(getActivity());
        }
        if (savedInstanceState.getParcelable("PredefMessageViewModel") != null) {
            this.mPredefMessage = savedInstanceState.getParcelable("PredefMessageViewModel");
            this.mPredefMessage.setContext(getActivity());
        }
    }

    public void postCreateViewHandler(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            this.reconstructStateInstance(savedInstanceState);
        } else {
            Log.d(TAG, "postCreateViewHandler: initializeinstance");
            this.initialiazeInstance();
        }
    }

    public void initialiazeInstance() {
        final MessageViewModel viewModel = new MessageViewModel(getActivity()) {

            @Override
            public void loadMoreItem() {
                ChatDetailFragment.this.getMessages();
            }

        };
        //this.mData = new HashMap<>();

        this.mMessage = viewModel;
        this.mProduct = new ProductViewModel(getActivity());
        this.mPredefMessage = new PredefinedMessageViewModel(getActivity());

        this.getMessages();
        this.getProductInfo();
        this.setPredefinedMessage();
    }

    //protected void bindData(Bundle args){
/*        this.setPageTitle();
        this.resetMenuFragment();*/
        //super.bindData(args);

        /*this.mFilter = new ArrayList<>();

        ODataQueryOption isActiveFilter = new ODataQueryOption();
        isActiveFilter.columnName = "IsActive";
        isActiveFilter.operator = ODataOperator.equals;
        isActiveFilter.value = "true";
        this.mFilter.add(isActiveFilter);*/
        //this.mData = new HashMap<>();
   // }

    public void setBtnListeners(View view){

        ChatActivity act = (ChatActivity) this.getActivity();

        // Listeners
        EditText editText = (EditText) view.findViewById(R.id.editMessage);
        Button btnSend = (Button) view.findViewById(R.id.btnSend);
        ImageButton btnImage = (ImageButton) view.findViewById(R.id.btnImage);
        ImageButton btnShowPredef = (ImageButton) view.findViewById(R.id.btnShowPredef);
        Button btnMakeOffer = (Button) view.findViewById(R.id.makeOfferBtn);
        Button btnViewSeller = (Button) view.findViewById(R.id.viewSellerBtn);

        btnSend.setOnClickListener(v -> {
            boolean hasText = !editText.getText().toString().trim().equals("");
            boolean hasImage = mMessage.getHasImage();

            if(hasText || hasImage){
                String message = editText.getText().toString();

                if(!hasImage) {
                    long tempID = mMessage.getItems().size() != 0 ? mMessage.getItems().getFirst().MessageID + 1 : 0;

                    Message pendingMessage = new Message();
                    pendingMessage.Status = "P";
                    pendingMessage.Message = message;
                    pendingMessage.IsMine = true;
                    pendingMessage.TimeStamp = "--";
                    pendingMessage.MessageID = tempID;
                    pendingMessage.TemporaryID = tempID;

                    mMessage.addItemFirst(pendingMessage);

                    act.chatService.Send(User.CurrentRoom, message, tempID);

                    editText.setText("");
                }
                else{
                    if(act.chatService.connectionState == ConnectionState.Connected) {
                        //TODO: Handle pending message
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        mMessage.getFirstBitmap().compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        String encodedImg = Base64.encodeToString(byteArray, Base64.DEFAULT);
                        mMessage.removeBitmaps();

                        act.chatService.SendImage(User.CurrentRoom, message, encodedImg);
                        editText.setText("");
                    }
                    else{
                        Toast.makeText(getActivity(), "No Internet Connection!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnShowPredef.setOnClickListener(v ->{

            final AlertDialog.Builder dialogBuilder =
                    new AlertDialog.Builder(new ContextThemeWrapper(act, R.style.AppTheme));

            LayoutInflater inflater = LayoutInflater.from(act);
            DialogPredefinedMessagesListBinding dataBindingHolder = DataBindingUtil.inflate(
                    inflater,
                    R.layout.dialog_predefined_messages_list,
                    null, false);
            dataBindingHolder.setPredefViewModel(this.mPredefMessage);

            final View dialogView = dataBindingHolder.getRoot();
            dialogBuilder.setView(dialogView);

            final AlertDialog dialog = dialogBuilder
                    .setTitle("PLACEHOLDER TITLE")
                    .setView(dialogView)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton)
                        {
                            List<PredefinedMessage> list = mPredefMessage.getItems();
                            for (PredefinedMessage message : list){
                                if (message.isSelected){
                                    editText.setText(message.Message);
                                }
                            }
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int whichButton)
                        {
                            dialog.dismiss();
                        }
                    }).create();
            dialog.show();
        } );

        btnImage.setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(act, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);

                if (intent.resolveActivity(v.getContext().getPackageManager()) != null) {
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), Globals.PICK_IMAGE);
                }
            }
            else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        Globals.PERMISSION_EXTERNAL);
            }
        });

        btnMakeOffer.setOnClickListener(v ->{

            final AlertDialog.Builder dialogBuilder =
                    new AlertDialog.Builder(new ContextThemeWrapper(act, R.style.AppTheme));

            LayoutInflater inflater = LayoutInflater.from(act);
            DialogMakeOfferBinding dataBindingHolder = DataBindingUtil.inflate(
                    inflater,
                    R.layout.dialog_make_offer,
                    null, false);
            dataBindingHolder.setProductViewModel(this.mProduct);

            final View dialogView = dataBindingHolder.getRoot();
            dialogBuilder.setView(dialogView);

            final AlertDialog dialog = dialogBuilder
                    .setTitle("PLACEHOLDER TITLE")
                    .setView(dialogView)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1)
                        {

                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int whichButton)
                        {
                            dialog.dismiss();
                        }
                    }).create();

            dialog.show();
        } );
    }

    public void handleAddMsg() {
        ChatActivity act = (ChatActivity) this.getActivity();
        Handler handler = new Handler(Looper.getMainLooper());

        if(act.chatService!=null) {
            act.chatService.proxy.on("addMessage", new SubscriptionHandler1<Message>() {
                @Override
                public void run(final Message msg) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            msg.IsMine = msg.SenderID == User.UserID;

                            if (msg.IsMine) {
                                int position = 0;
                                for (Message m : mMessage.getItems()) {
                                    if (m.TemporaryID == msg.TemporaryID) {
                                        mMessage.getItems().set(position, msg);
                                        mMessage.notifyPropertyChanged(position);
                                        break;
                                    }
                                    position++;
                                }
                            } else {
                                ChatMessageAdapter adapter = (ChatMessageAdapter) mRecyclerView.getAdapter();

                                mMessage.addItemFirst(msg);
                                if (adapter.mLastViewedPosition < 20) {
                                    mRecyclerView.smoothScrollToPosition(0);
                                }

                                mRecyclerView.getAdapter().notifyItemInserted(0);
                            }
                            //Set other's messages as Read
                            if (!msg.IsMine)
                                MessagesRead();
                        }
                    });
                }
            }, Message.class);

            act.chatService.proxy.on("setMessageRead", new SubscriptionHandler1<Long[]>() {
                @Override
                public void run(Long[] msgID) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mMessage.setToRead(msgID);
                            //notifyAdapter();
                        }
                    });
                }
            }, Long[].class);
        }
    }

    private void handleConnectionState(){
        ChatActivity act = (ChatActivity) this.getActivity();

        //from https://github.com/SignalR/java-client/issues/92
        act.chatService.connection.stateChanged(new StateChangedCallback() {
            @Override
            public void stateChanged(ConnectionState oldState, ConnectionState newState) {
                try {
                    Timer timer = new Timer();

                    TimerTask reconnectTask = new TimerTask() {
                        @Override
                        public void run() {
                            Log.d("signalrconnection d","ATTEMPTING RECONNECT");
                            try{
                                ClientTransport clientTransport = new ServerSentEventsTransport(act.chatService.connection.getLogger());
                                act.chatService.connection.start(clientTransport).get();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                                Log.d("signalrconnection d","RECONNECT FAIL1: " + e.getMessage());
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                                Log.d("signalrconnection d","RECONNECT FAIL2: " + e.getMessage());
                            } catch (Exception e){
                                e.printStackTrace();
                                Log.d("signalrconnection d","RECONNECT FAIL3: " + e.getMessage());
                            }
                        }
                    };

                    act.chatService.connectionState = newState;

                    if (newState == ConnectionState.Connected) {
                        Log.d("signalrconnection d", "STATE CHANGED-CONNECTED!");
                        if(oldState == ConnectionState.Connecting) {
                            Log.d("signalrconnection d", "SUCCESS RECONNECTED!");
                            timer.cancel();
                            ChatMessageAdapter adapter = (ChatMessageAdapter) mRecyclerView.getAdapter();
                            if (adapter != null)
                                adapter.resetLastPosition();
                            afterReconnect();
                        }
                    } else if (newState == ConnectionState.Reconnecting) {
                        Log.d("signalrconnection d", "STATE CHANGED-RECONNECTING!");
                        // should we reconnect here or on disconnect instead?
                        timer.scheduleAtFixedRate(reconnectTask, 1000, 5000);act.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(act, "No connection! Reconnecting...", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else if (newState == ConnectionState.Disconnected) {
                        Log.d("signalrconnection d", "STATE CHANGED-DISCONNECTED!");
                    }
                }
                catch (Exception e){
                    Log.d("signalrconnection d", "EXCEPTION: " + e.getMessage() + "|| STATE: " + newState.name());
                }
            }
        });
    }

    private void afterReconnect(){
        ChatActivity act = (ChatActivity) this.getActivity();

        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(act, "Connection re-established!", Toast.LENGTH_LONG).show();
            }
        });

        act.chatService.Join(User.CurrentRoom);

        handleAddMsg();
        List<Message> messageList = mMessage.getItems();

        for (Message m : messageList) {
            if (!m.IsMine) {
                //get new messages based on the latest ID of the other's message
                reconnectGetMessages(m.MessageID);
                break;
            }
        }

        //Resend all pending messages
        ArrayList<Message> messagesToSend = new ArrayList<>();
        for (Message m : messageList) {
            if (m.Status.equals("P")) {
                messagesToSend.add(m);
            }
        }
        Collections.reverse(messagesToSend);
        for (Message m : messagesToSend) {
            try {
                //Send the messages every 0.3 second to prevent ordering mixup
                act.chatService.Send(User.CurrentRoom, m.Message, m.TemporaryID);
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Globals.PERMISSION_EXTERNAL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);

                    if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Globals.PICK_IMAGE);
                    }
                } else {
                    // permission denied
                    Toast.makeText(getActivity(), "Permission needed to send image!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Globals.PICK_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    if(data.getData()!=null){
                        Uri originalUri = data.getData();

                        ContentResolver cR = getActivity().getContentResolver();

                        File file = FileUtils.getFile(getActivity(), originalUri);

                        InputStream inputStream = cR.openInputStream(data.getData());
                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                        mMessage.removeBitmaps();
                        mMessage.addBitmap(bitmap);
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getMessages() {
        ChatActivity act = (ChatActivity) this.getActivity();

        if(act.chatService!=null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    act.chatService.proxy.invoke(Message[].class, "GetMessageHistory", User.CurrentRoom, mCurrentPage).done(
                            new Action<Message[]>() {
                                @Override
                                public void run(Message[] messageViewModels) throws Exception {
                                    for (Message m : messageViewModels) {
                                        m.IsMine = m.SenderID == User.UserID;
                                    }

                                    if (mCurrentPage == 1) {
                                        LinkedList<Message> messageArrayList = new LinkedList<Message>();
                                        messageArrayList.addAll(Arrays.asList(messageViewModels));
                                        mMessage.setItems(messageArrayList);
                                    } else {
                                        mMessage.addItems(Arrays.asList(messageViewModels));

                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                ChatMessageAdapter adapter = (ChatMessageAdapter) mRecyclerView.getAdapter();
                                                if (adapter != null)
                                                    adapter.notifyDataSetChanged();
                                            }
                                        });
                                    }

                                    if (Array.getLength(messageViewModels) > 0) {
                                        mCurrentPage += 1;
                                    }

                                    //Set other's messages as Read
                                    MessagesRead();
                                }
                            });
                    return null;
                }
            }.execute();
        }
    }

    private void reconnectGetMessages(long lastMessageID) {
        ChatActivity act = (ChatActivity) this.getActivity();

        if(act.chatService!=null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    act.chatService.proxy.invoke(Message[].class, "ReconnectGetMessageHistory", User.CurrentRoom, User.UserID, lastMessageID).done(
                            new Action<Message[]>() {
                                @Override
                                public void run(Message[] messageViewModels) {
                                    if (messageViewModels.length > 0) {
                                        for (Message m : messageViewModels) {
                                            m.IsMine = m.SenderID == User.UserID;
                                            mMessage.addItemFirst(m);
                                        }
                                        //notifyAdapter();

                                        //Set other's messages as Read
                                        MessagesRead();
                                    }
                                }
                            });
                    return null;
                }
            }.execute();
        }
    }

    private void getProductInfo(){
        ChatActivity act = (ChatActivity) this.getActivity();

        if(act.chatService!=null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    act.chatService.proxy.invoke(Product.class, "GetProductInRoom", User.CurrentRoom).done(
                            new Action<Product>() {
                                @Override
                                public void run(Product product) throws Exception {
                                    mProduct.setItemModel(product);
                                    mProduct.notifyPropertyChanged(BR._all);
                                    /*Globals.Messages.clear();
                                    Globals.Messages.addAll(Arrays.asList(messageViewModels));*/
                                    //if (Array.getLength(messageViewModels) != 0) {
                                }
                            });
                    return null;
                }
            }.execute();
        }
    }

    private void setPredefinedMessage(){
        //Hardcoded predefined message. After merge, call API for this
        ArrayList<PredefinedMessage> predefinedMessageList = new ArrayList<>();

        PredefinedMessage predef1 = new PredefinedMessage();
        predef1.PredefinedMessageID = 17;
        predef1.Message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore";
        predef1.Type = "A";
        predefinedMessageList.add(predef1);
        for (int i=1;i<15;i++)
        {
            PredefinedMessage predefLoop = new PredefinedMessage();
            predefLoop.PredefinedMessageID = i;
            predefLoop.Message = "Message #" + i;
            predefLoop.Type = "A";
            predefinedMessageList.add(predefLoop);
        }
        PredefinedMessage predef2 = new PredefinedMessage();
        predef2.PredefinedMessageID = 16;
        predef2.Message = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. ";
        predef2.Type = "A";
        predefinedMessageList.add(predef2);

        this.mPredefMessage.setItems(predefinedMessageList);
    }

    public void MessagesRead(){
        ChatActivity act = (ChatActivity) this.getActivity();

        if (act != null && act.chatService != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    act.chatService.proxy.invoke("MessagesRead", User.CurrentRoom, User.UserID);
                    return null;
                }
            }.execute();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        this.generateBufferStateInstance(outState);
    }

    /*@Override*/
    public void generateBufferStateInstance(Bundle outState) {
        outState.putParcelable("MessageViewModel", this.mMessage);
        outState.putParcelable("ProductViewModel", this.mProduct);
        outState.putParcelable("PredefMessageViewModel", this.mPredefMessage);
    }
}
