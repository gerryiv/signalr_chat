package com.chat.akouki.chatmobile.viewmodel;

import android.content.Context;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.chat.akouki.chatmobile.BR;
import com.chat.akouki.chatmobile.R;
import com.chat.akouki.chatmobile.modelviews.ChatRoom;
import com.chat.akouki.chatmobile.modelviews.Product;

public class ProductViewModel extends BaseViewModel implements Parcelable {

    public ProductViewModel(Context context) {
        super(context);
    }

    public ProductViewModel(Context context, Product itemModel) {
        super(context);

        this.setItemModel(itemModel);
    }

    public ProductViewModel(Parcel in) {

        super(null);

        this.setItemModel((Product) in.readValue(ProductViewModel.class.getClassLoader()));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.getItemModel());
    }

    public static final Creator CREATOR = new Creator() {
        public ProductViewModel createFromParcel(Parcel in) {
            return new ProductViewModel(in);
        }

        public ProductViewModel[] newArray(int size) {
            return new ProductViewModel[size];
        }
    };

    @Bindable
    public Product itemModel;

    public Product getItemModel() {
        return this.itemModel;
    }

    public void setItemModel(Product productItemModel) {
        this.itemModel = productItemModel;
        this.notifyPropertyChanged(BR._all);
    }

    private boolean keyboardDown = true;

    @Bindable
    public boolean getKeyboardDown(){
        return keyboardDown;
    }

    public void setKeyboardDown(boolean bool){
        this.keyboardDown = bool;
        this.notifyChange();
    }

    @Bindable
    public long getID() {
        Product product = this.getItemModel();
        return product.ProductID;
    }

    @Bindable
    public String getName() {
        Product product = this.getItemModel();

        if(product == null)
            return "Loading..";

        return product.Name;
    }

    @Bindable
    public String getPrice() {
        Product product = this.getItemModel();

        if(product == null)
            return "Loading..";

        return "Rp. " + String.valueOf(product.Price);
    }

    @Bindable
    public String getCondition(){
        Product product = this.getItemModel();

        if(product == null)
            return "Loading..";

        return product.Condition;
    }

    @Bindable
    public String getPriceDialogText(){
        Product product = this.getItemModel();

        if(product == null)
            return "Loading..";

        return product.SellerName + " is selling at Rp." + String.valueOf(product.Price);
    }

    @BindingAdapter({"offerStatus"})
    public static void setOfferStatus(TextView textView, ProductViewModel model) {
        Context context = textView.getContext();

        textView.setText("PLACEHOLDER");
        textView.setTextColor(context.getResources().getColor(R.color.white));
        textView.setBackgroundColor(context.getResources().getColor(R.color.productStatusSoldOut));
    }
}
