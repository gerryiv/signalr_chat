package com.chat.akouki.chatmobile.modelviews;

import java.io.Serializable;

public class Product implements Serializable {
    public String SellerName;
    public long ProductID;
    public String Name;
    public long Price;
    public String Condition;
    public String Picture;
}

