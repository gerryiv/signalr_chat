package com.chat.akouki.chatmobile.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.chat.akouki.chatmobile.R;
import com.chat.akouki.chatmobile.databinding.ItemChatroomBinding;
import com.chat.akouki.chatmobile.databinding.ItemMessageMineBinding;
import com.chat.akouki.chatmobile.modelviews.ChatRoom;
import com.chat.akouki.chatmobile.modelviews.Message;
import com.chat.akouki.chatmobile.viewmodel.ItemChatRoomViewModel;
import com.chat.akouki.chatmobile.viewmodel.ItemMessageViewModel;

import java.util.List;

public class ChatRoomAdapter extends RecyclerView.Adapter<ChatRoomAdapter.BindingHolder> {

    private List<ChatRoom> mItems;
    private Context mContext;
    private int mLastPosition;

    public ChatRoomAdapter(Context ctx, List<ChatRoom> dataItems) {
        this.mItems = dataItems;
        this.mContext = ctx;
        this.mLastPosition = 0;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        BindingHolder holder = null;

        final ItemChatroomBinding postBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_chatroom,
                parent,
                false);

        holder = new BindingHolder(postBinding);

        return holder;
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ViewDataBinding itemBinding = holder.binding;

        ChatRoom roomModel = mItems.get(position);

        ItemChatRoomViewModel itemViewModel = new ItemChatRoomViewModel(mContext, roomModel);
        ((ItemChatroomBinding) itemBinding).setPosition(position);
        ((ItemChatroomBinding) itemBinding).setViewModel(itemViewModel);

        int count = getItemCount() - 1;

        //count needs to be 15 or more so it wont keep triggering refresh if it only has a few rooms
        if (position == count && this.mLastPosition != position && count > 15) {
            this.mLastPosition = position;
            this.loadMoreItem();
        }

        itemBinding.executePendingBindings();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BindingHolder(ItemChatroomBinding binding) {
            super(binding.chatroomItemMainContainer);
            this.binding = binding;
        }
    }

    protected void loadMoreItem() {
        //Handle in UI
    }
}
