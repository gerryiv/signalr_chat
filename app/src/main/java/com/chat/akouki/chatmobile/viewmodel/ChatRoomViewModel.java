package com.chat.akouki.chatmobile.viewmodel;


import android.content.Context;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.chat.akouki.chatmobile.BR;
import com.chat.akouki.chatmobile.adapters.ChatMessageAdapter;
import com.chat.akouki.chatmobile.adapters.ChatRoomAdapter;
import com.chat.akouki.chatmobile.modelviews.ChatRoom;
import com.chat.akouki.chatmobile.modelviews.Message;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ChatRoomViewModel extends BaseViewModel implements Parcelable {

    public ChatRoomViewModel(Context context) {
        super(context);
        this.mContext = context;
    }

    protected ChatRoomViewModel(Parcel in) {
        super(null);

        this.setItems((LinkedList<ChatRoom>) in.readValue(Collection.class.getClassLoader()));
    }

    public static final Creator CREATOR = new Creator() {

        public ChatRoomViewModel createFromParcel(Parcel in) {return new ChatRoomViewModel(in);}

        public ChatRoomViewModel[] newArray(int size) {return new ChatRoomViewModel[size];}
    };


    @Bindable
    private LinkedList<ChatRoom> items;

    public LinkedList<ChatRoom> getItems() {
        return items;
    }

    public void setItems(LinkedList<ChatRoom> data){
        this.items = data;
        this.notifyChange();
    }

    public void addItemsFirst(List<ChatRoom> data){
        for (ChatRoom m: data ) {
            this.getItems().addFirst(m);
        }
        this.notifyChange();
    }

    public void addItems(List<ChatRoom> data){
        for (ChatRoom m: data ) {
            this.getItems().add(m);
        }
        //this.notifyChange();
    }

    public void loadMoreItem() {
        //Override in UI
    }

    @BindingAdapter({"android:roomItems"})
    public static void bindRoom(RecyclerView view, final ChatRoomViewModel model) {
        view.setHasFixedSize(false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //layoutManager.setReverseLayout(true);
        view.setLayoutManager(layoutManager);

        if (model.getItems() != null) {
            view.setAdapter(new ChatRoomAdapter(view.getContext(), model.getItems()) {
                @Override
                protected void loadMoreItem() {
                    model.loadMoreItem();
                }
            });
            view.scrollToPosition(0);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.getItems());
    }
}
