package com.chat.akouki.chatmobile.modelviews;

import java.io.Serializable;

public class PredefinedMessage implements Serializable {
    public long PredefinedMessageID;
    public String Message;
    public String Type;
    public boolean isSelected;
}
